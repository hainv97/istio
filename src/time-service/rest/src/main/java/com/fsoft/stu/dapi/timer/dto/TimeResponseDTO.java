package com.fsoft.stu.dapi.timer.dto;

import java.time.ZonedDateTime;

import lombok.Data;

@Data
public class TimeResponseDTO {

	private ZonedDateTime zonedDateTime;

}
