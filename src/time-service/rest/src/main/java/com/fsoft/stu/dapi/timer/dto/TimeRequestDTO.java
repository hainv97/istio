package com.fsoft.stu.dapi.timer.dto;

import java.time.ZoneId;

import lombok.Data;

@Data
public class TimeRequestDTO {
	
    private ZoneId zoneId;
	
}
