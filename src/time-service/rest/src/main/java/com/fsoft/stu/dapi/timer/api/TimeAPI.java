package com.fsoft.stu.dapi.timer.api;


import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fsoft.stu.dapi.timer.dto.TimeRequestDTO;
import com.fsoft.stu.dapi.timer.dto.TimeResponseDTO;

/**
 * @author DuongPTH
 *
 */
@RequestMapping(
		path = TimeAPI.BASE, 
		produces = TimeAPI.MEDIA_TYPE, 
		consumes = TimeAPI.MEDIA_TYPE)
public interface TimeAPI {

    String BASE = "/time";
    String MEDIA_TYPE = MediaType.APPLICATION_JSON_UTF8_VALUE;

    /**
     * @return
     */
    @PostMapping
    ResponseEntity<TimeResponseDTO> getTime(@RequestBody TimeRequestDTO requestDTO);

}
