package com.fsoft.stu.dapi.timer.service;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.stereotype.Service;

@Service
public class TimerService {

	public ZonedDateTime getTime(ZoneId zoneId) {
        return ZonedDateTime.now(zoneId);
    }

}
