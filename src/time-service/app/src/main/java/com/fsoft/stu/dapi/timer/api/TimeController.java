package com.fsoft.stu.dapi.timer.api;

import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.stu.dapi.timer.dto.TimeRequestDTO;
import com.fsoft.stu.dapi.timer.dto.TimeResponseDTO;
import com.fsoft.stu.dapi.timer.service.TimerService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class TimeController implements TimeAPI {

	private TimerService timerService;
	
	public TimeController(TimerService timerService) {
		super();
		this.timerService = timerService;
	}

	@Override
	public ResponseEntity<TimeResponseDTO> getTime(@RequestBody TimeRequestDTO requestDTO) {
		log.info("-------------------------------------------");
    	log.info("TimeRequestDTO : " + requestDTO);
    	
		ZonedDateTime now = timerService.getTime(requestDTO.getZoneId());
    	TimeResponseDTO responseDTO = new TimeResponseDTO();
    	responseDTO.setZonedDateTime(now);
    	log.info("TimeResponseDTO : " + responseDTO);
    	
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
	}

}