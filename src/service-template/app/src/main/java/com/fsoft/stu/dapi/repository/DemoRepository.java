package com.fsoft.stu.dapi.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.stu.dapi.domain.DemoDomainEntity;

public interface DemoRepository extends JpaRepository<DemoDomainEntity, Long> {

    Page<DemoDomainEntity> findByContentIgnoreCaseContaining(String content, Pageable pageable);

}
