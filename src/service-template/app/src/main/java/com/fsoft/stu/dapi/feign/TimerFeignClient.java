package com.fsoft.stu.dapi.feign;

import org.springframework.cloud.openfeign.FeignClient;

import com.fsoft.stu.dapi.timer.api.TimeAPI;

@FeignClient(
		name = "${service.provider.name}", 
		url = "${service.provider.url}")
public interface TimerFeignClient extends TimeAPI {

}
