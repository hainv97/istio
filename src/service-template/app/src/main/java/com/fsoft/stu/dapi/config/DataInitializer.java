package com.fsoft.stu.dapi.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fsoft.stu.dapi.domain.DemoDomainEntity;
import com.fsoft.stu.dapi.repository.DemoRepository;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class DataInitializer {

    @Bean
    CommandLineRunner initDatabase(DemoRepository repository) {
        return args -> {
            log.info("Preloading " + repository.save(new DemoDomainEntity("Digital Transformation")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Infrastructure")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Internet of Things")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Artifical Intelligent")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Blockchain")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Automotive")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Cloud")));
            log.info("Preloading " + repository.save(new DemoDomainEntity("Dev-Ops")));
        };
    }

}
