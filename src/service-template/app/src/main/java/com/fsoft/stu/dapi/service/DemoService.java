package com.fsoft.stu.dapi.service;

import org.springframework.data.domain.Page;

import com.fsoft.stu.dapi.domain.DemoDomainEntity;

public interface DemoService extends BaseService<DemoDomainEntity, Long> {

    Page<DemoDomainEntity> search(String searchKey, int pageIndex, int pageSize);

}