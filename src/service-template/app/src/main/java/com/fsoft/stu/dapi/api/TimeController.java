package com.fsoft.stu.dapi.api;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.stu.dapi.dto.request.DemoRequestDTO;
import com.fsoft.stu.dapi.dto.response.DemoResponseDTO;
import com.fsoft.stu.dapi.feign.TimerFeignClient;
import com.fsoft.stu.dapi.mapper.DemoMapper;
import com.fsoft.stu.dapi.service.DemoService;
import com.fsoft.stu.dapi.timer.dto.TimeRequestDTO;
import com.fsoft.stu.dapi.timer.dto.TimeResponseDTO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(
        path = TimeController.BASE,
        consumes = TimeController.MEDIA_TYPE,
        produces = TimeController.MEDIA_TYPE)
public class TimeController {

    static final String BASE = "/demo-time";
    static final String MEDIA_TYPE = "application/vnd.stu.v1+json;charset=UTF-8";

    private DemoService demoService;
    private DemoMapper demoMapper;
    private TimerFeignClient timerFeignClient;

    public TimeController(
    		DemoService demoService, 
    		DemoMapper demoMapper, 
    		TimerFeignClient timerFeignClient) {
    	
        super();
        this.demoService = demoService;
        this.demoMapper = demoMapper;
        this.timerFeignClient = timerFeignClient;
    }

    @PostMapping
    public ResponseEntity<DemoResponseDTO> createWithTime(@RequestBody DemoRequestDTO demoRequestDTO) {
    	log.info("-------------------------------------------");
    	log.info("DemoRequestDTO : " + demoRequestDTO);
        
        DemoResponseDTO demoResponseDTO = Optional.of(demoRequestDTO)
                .map(demoMapper::toEntity)
                .map(demoService::create)
                .map(demoMapper::toResponseDTO)
                .get();

        TimeRequestDTO timeRequestDTO = new TimeRequestDTO();
        timeRequestDTO.setZoneId(demoRequestDTO.getZoneId());
        log.info("TimeRequestDTO : " + timeRequestDTO);
        TimeResponseDTO timeResponseDTO = timerFeignClient.getTime(timeRequestDTO).getBody();
        log.info("TimeResponseDTO : " + timeResponseDTO);
        demoResponseDTO.setDateTime(timeResponseDTO.getZonedDateTime());
        log.info("DemoResponseDTO : " + demoResponseDTO);
        
        return new ResponseEntity<>(demoResponseDTO, HttpStatus.CREATED);
    }
    
}
