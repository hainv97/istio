package com.fsoft.stu.dapi.service;

import java.util.List;
import java.util.Optional;

public interface BaseService<T, ID> {

    T create(T entity);

    Optional<T> read(ID id);

    List<T> readAll();

    T update(T entity);

    void delete(ID id);

}
