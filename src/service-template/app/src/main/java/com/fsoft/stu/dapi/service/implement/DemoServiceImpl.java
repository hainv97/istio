package com.fsoft.stu.dapi.service.implement;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fsoft.stu.dapi.domain.DemoDomainEntity;
import com.fsoft.stu.dapi.repository.DemoRepository;
import com.fsoft.stu.dapi.service.DemoService;

@Service
public class DemoServiceImpl
        extends BaseServiceImpl<DemoDomainEntity, Long>
        implements DemoService {

    private DemoRepository demoRepository;

    public DemoServiceImpl(DemoRepository demoRepository) {
        super(demoRepository);
        this.demoRepository = demoRepository;
    }

    @Override
    public Page<DemoDomainEntity> search(String searchKey, int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize);
        return demoRepository.findByContentIgnoreCaseContaining(searchKey, pageable);
    }

}
