package com.fsoft.stu.dapi.service.implement;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fsoft.stu.dapi.service.BaseService;

public class BaseServiceImpl<T, ID> implements BaseService<T, ID> {

    private JpaRepository<T, ID> repository;

    protected BaseServiceImpl(JpaRepository<T, ID> repository) {
        this.repository = repository;
    }

    @Override
    public T create(T entity) {
        return repository.save(entity);
    }

    @Override
    public Optional<T> read(ID id) {
        return repository.findById(id);
    }

    @Override
    public List<T> readAll() {
        return repository.findAll();
    }

    @Override
    public T update(T entity) {
        return repository.save(entity);
    }

    @Override
    public void delete(ID id) {
        repository.deleteById(id);
    }

}
