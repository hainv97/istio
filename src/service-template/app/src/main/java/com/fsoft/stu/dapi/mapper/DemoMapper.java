package com.fsoft.stu.dapi.mapper;


import com.fsoft.stu.dapi.domain.DemoDomainEntity;
import com.fsoft.stu.dapi.dto.request.DemoRequestDTO;
import com.fsoft.stu.dapi.dto.response.DemoResponseDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface DemoMapper {

    DemoDomainEntity toEntity(DemoRequestDTO dto);

    DemoResponseDTO toResponseDTO(DemoDomainEntity entity);

}