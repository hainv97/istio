package com.fsoft.stu.dapi.api;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fsoft.stu.dapi.dto.request.DemoRequestDTO;
import com.fsoft.stu.dapi.dto.response.DemoResponseDTO;
import com.fsoft.stu.dapi.mapper.DemoMapper;
import com.fsoft.stu.dapi.service.DemoService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class DemoController implements DemoAPI {

    private DemoService demoService;
    private DemoMapper demoMapper;

    public DemoController(
    		DemoService demoService, 
    		DemoMapper demoMapper) {
    	
        super();
        this.demoService = demoService;
        this.demoMapper = demoMapper;
    }

    @Override
    public ResponseEntity<DemoResponseDTO> create(@RequestBody DemoRequestDTO requestDTO) {
        log.info("create " + requestDTO.toString());
        DemoResponseDTO responseDTO = Optional.of(requestDTO)
                .map(demoMapper::toEntity)
                .map(demoService::create)
                .map(demoMapper::toResponseDTO)
                .get();
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<DemoResponseDTO>> getAll() {
        log.info("get all");
        List<DemoResponseDTO> responseDTOs = demoService.readAll().stream()
                .map(demoMapper::toResponseDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(responseDTOs, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<DemoResponseDTO> getOne(@PathVariable Long id) {
        log.info("get one with id=" + id);
        return demoService.read(id)
                .map(demoMapper::toResponseDTO)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<DemoResponseDTO> update(@RequestBody DemoRequestDTO requestDTO, @PathVariable Long id) {
        log.info("update one with id=" + id + " | " + requestDTO.toString());
        if (!demoService.read(id).isPresent())
            return ResponseEntity.notFound().build();
        return Optional.of(requestDTO)
                .map(e -> {
                    e.setId(id);
                    return e;
                })
                .map(demoMapper::toEntity)
                .map(demoService::update)
                .map(demoMapper::toResponseDTO)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.badRequest().build());
    }

    @Override
    public ResponseEntity<?> delete(@PathVariable Long id) {
        log.info("delete one with id=" + id);
        if (!demoService.read(id).isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        demoService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<List<DemoResponseDTO>> search(
            @PathVariable String searchKey,
            @PathVariable int pageIndex,
            @PathVariable int pageSize) {

        log.info(String.format("search \"%s\" / %d / %d", searchKey, pageIndex, pageSize));
        List<DemoResponseDTO> responseDTOs = demoService
                .search(searchKey, pageIndex, pageSize)
                .getContent()
                .stream()
                .map(demoMapper::toResponseDTO)
                .collect(Collectors.toList());
        return new ResponseEntity<>(responseDTOs, HttpStatus.OK);
    }

}