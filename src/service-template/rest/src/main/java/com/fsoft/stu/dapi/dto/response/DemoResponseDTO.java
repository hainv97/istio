package com.fsoft.stu.dapi.dto.response;

import java.time.ZonedDateTime;

import lombok.Data;

@Data
public class DemoResponseDTO {

    private Long id;
    private String content;
    private ZonedDateTime dateTime;

}
