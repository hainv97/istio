package com.fsoft.stu.dapi.dto.request;

import java.time.ZoneId;

import lombok.Data;

@Data
public class DemoRequestDTO {
	
	private Long id;
	private String content;
	private ZoneId zoneId;
	
}