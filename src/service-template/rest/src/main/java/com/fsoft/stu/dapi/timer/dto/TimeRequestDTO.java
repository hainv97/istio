package com.fsoft.stu.dapi.timer.dto;

import lombok.Data;

import java.time.ZoneId;

@Data
public class TimeRequestDTO {
	
    private ZoneId zoneId;
	
}
