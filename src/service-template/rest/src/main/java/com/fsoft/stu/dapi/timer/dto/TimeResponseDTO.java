package com.fsoft.stu.dapi.timer.dto;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class TimeResponseDTO {

	private ZonedDateTime zonedDateTime;

}
