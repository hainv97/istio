package com.fsoft.stu.dapi.api;


import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fsoft.stu.dapi.dto.request.DemoRequestDTO;
import com.fsoft.stu.dapi.dto.response.DemoResponseDTO;

@RequestMapping(
        path = DemoAPI.BASE,
        consumes = DemoAPI.MEDIA_TYPE,
        produces = DemoAPI.MEDIA_TYPE)
public interface DemoAPI {

    String BASE = "/demo";
    String MEDIA_TYPE = "application/vnd.stu.v1+json;charset=UTF-8";

    @PostMapping
    ResponseEntity<DemoResponseDTO> create(@RequestBody DemoRequestDTO requestDTO);

    @GetMapping
    ResponseEntity<List<DemoResponseDTO>> getAll();

    @GetMapping("/{id}")
    ResponseEntity<DemoResponseDTO> getOne(@PathVariable Long id);

    @PutMapping("/{id}")
    ResponseEntity<DemoResponseDTO> update(@RequestBody DemoRequestDTO requestDTO, @PathVariable Long id);

    @DeleteMapping("/{id}")
    ResponseEntity<?> delete(@PathVariable Long id);

    @GetMapping("/search/{searchKey}/{pageIndex}/{pageSize}")
    ResponseEntity<List<DemoResponseDTO>> search(
            @PathVariable String searchKey,
            @PathVariable int pageIndex,
            @PathVariable int pageSize);

}
