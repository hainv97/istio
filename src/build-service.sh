set -o errexit

if [ "$#" -ne 1 ]; then
    echo Missing version parameter
    echo Usage: build-services.sh \<version\>
    exit 1
fi

VERSION=$1
SCRIPTDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

pushd "$SCRIPTDIR/time-service"
  docker build -t "asia.gcr.io/thinh-221414/time:${VERSION}" .
popd

pushd "$SCRIPTDIR/service-template"
  docker build -t "asia.gcr.io/thinh-221414/demo:${VERSION}" .
popd
