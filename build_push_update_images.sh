set -o errexit

if [ "$#" -ne 1 ]; then
    echo Missing version parameter
    echo Usage: build-services.sh \<version\>
    exit 1
fi

VERSION=$1
src/build-service.sh "${VERSION}"

for v in ${VERSION} "latest"
do
  IMAGES+=$(docker images -f reference=asia.gcr.io/thinh-221414/*:"$v" --format "{{.Repository}}:$v")
done

for IMAGE in ${IMAGES}; do docker push "${IMAGE}"; done

